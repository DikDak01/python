
#Dieses Modul kann Grafiken mithilfe einfachen Befehlen erstellen
from turtle import *

#-----------------------------------------------------------------------------------------------------#

bgcolor('pink')
color('red')

begin_fill()
pensize(4)
left(50)
forward(133)
circle(50, 200)
right(140)
circle(50, 200)
left(140)
end_fill()