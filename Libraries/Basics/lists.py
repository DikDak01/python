#Listen, Tupel, Sets und Dictionaries

#Listen: Der wichtigste Datentyp zur Speicherung einer variabeln
#Anzahl von Elementen sind Listen. Es kann an jeder beliebigen
#Stelle Elemente hinzugefügt werden.

#Tupel (Sequenzen): Ein Tupel ist eine unveränderliche Liste
#mit wenigen Elementen. Tupels sind simpler als Listen.

#Sets (Mengen): Ein Set ist eine Menge von ungeordneten
#Elementen ohne Doppelgänger.

#Dictionaries (wörterbücher): In Dictionaries können Sie Wertpaare
#(Key-Value-Paare) speichern. Der wesentliche Unterschied im Vergleich
#zu Listen besteht darin, dass der Zugriff auf einzelne Werte
#über einen Schlüssel erfolgt, nicht über einen durchlaufenden
#Index.

#Arrays (Felder): In Python haben Arrays eine untergeordnete Rolle.
#Arrays können als Alternative zu Listen eingesetzt werden,
#wenn die Elementanzahl von vornherein bekannt ist.
#Arrays sind weniger flexibler als Listen, dafür aber bei
#vielen Elementen effizienter.

#-----------------------------------------------------------------------------------------------------#
#!Liste, Tumpel (einfache Liste, kann nicht geändert werden), Set
i, k, l = ['eine', 'Liste'], ('ein', 'Tupel'), {'ein', 'Set'}

print(i, k, l)

#-----------------------------------------------------------------------------------------------------#

#gewöhnliche Liste
list = [1 , 1.5, 'abc', 'cde', 4.5]

#Das 4 Elemente von list
print(list[3])

#vom dritten bis zum vierten Element
print(list[2:4])

#umgekehrte Reihenfolge
print(list[::-1])

#ändert das 2. Listenelement von 1.5 zu 3
list[1] = 3
print(list)

#Ein Element einer Liste hinzufügen
lst = [0, 1, 2, 3, 4] #~Liste
lst.extend([32]) #~ein Element zu einer Liste hinzufügen

print(lst)

#-----------------------------------------------------------------------------------------------------#

#range Funktion
#mit der Funktion list(range(start, ende, schrittweite)) wird eine unkomplizierte Liste erstellt

bsp1 = list[range[0, 1000, 10]] #TODO: Fehlerbehebung, object is not subscriptable

#-----------------------------------------------------------------------------------------------------#

#Listen und Zeichenketten
#Zeichen einer Zeichenketten mithilfe von list in eine Liste umwandeln

example_list = list["Hello cat"]


#-----------------------------------------------------------------------------------------------------#

#Zusammenfügen einer neuen Zeichenkette
example_join = '->'.join(example_list)

#-----------------------------------------------------------------------------------------------------#

#List Comprehension
#Anweisung in der Form von [ausdruck for x in liste]
bsp = list(range(10, 110, 10))
[x*2+1 for x in bsp]
