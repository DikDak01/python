#Datum und Uhrzeit mit Python

#native: Native Objekte geben einen Zeitpunkt an, ihnen fehlt aber
#die Kontextinformation, in welcher Zeitzone die Zeit gilt, ob
#Sommer- oder Winterzeit gilt

#aware: Zeitobjekte, die diese Zeitobjekte haben, werden
#aware genannt. Bei solchen Objekten ist eine Feststellung möglich,
#welcher Zeitpunkt absolut gesehen früher oder später ist.

#Die meisten Zeitobjekte sind unverändliche Typen (immutable).

#-----------------------------------------------------------------------------------------------------#

#Das Modul datetime hat wie jedes andere Modul verschiedene Klassen
#eines davon ist datetime. Nur import datetime würde den Tippaufwand
#erhöhen (date.datetime.now())
from datetime import datetime

#Hier wird von dem Modul datetime nur die Klasse date importiert
from datetime import date

#Das Modul pytz unterstütz fast alle Zeitzonen
import pytz

#Hier wird von dem Modul datetime nur die Klasse timedelta importiert
from datetime import timedelta

#-----------------------------------------------------------------------------------------------------#

#Zeit ermitteln und darstellen
now = datetime.now()
print(now)

#einzelne Zeitkomponenten ausgeben lassen

print(now.year)

print(now.month)

#Datum und Uhrzeit formatiert ausgeben

print(now.isoformat())

print(now.strftime('%d.%m.%Y, %H:%M'))

#Datum aus Zeichenkette einlesen (parsen)

t = '01.10.2022 20:01'
dt = datetime.strptime(t, '%d.%m.%Y %H:%M')

#-----------------------------------------------------------------------------------------------------#

#Nur das aktuelle Datum ausgeben
today3 = date.today()
print(today3)

#Mit dem datetime Objekt diese Aufgabe lösen
today2 = datetime.now()
print(now.date())

#Zeit ohne Datum
print(datetime.now().time())

today = datetime.today()
time = datetime.now().time()
#together = datetime.combine(date, time) #nochmals anschauen

#-----------------------------------------------------------------------------------------------------#

#Objekte für einen beliebigen Zeitpunkt
somedate = datetime(2022, 12, 31, 12, 0, 0)
print(somedate.isoformat())

#-----------------------------------------------------------------------------------------------------#

#Alle Zeitzonen abfragen die im Modul integriert sind
#print('Timezones')
#for timezone in pytz.all_timezone:
#        print(timezone)

#Zeiten mit Zeitzonen (Weltzeit abfragen)
utc = datetime.now(pytz.utc)
print(utc)

#Aktuelle Zeit in der befindlichen Zeitzone
localtime = utc.astimezone()
print(localtime)

#-----------------------------------------------------------------------------------------------------#

#Für ein naives Zeitobjekt (datetime.now) ist localize zu nutzen
zurich = pytz.timezone('Europe/Zurich')
zurichtime= zurich.localize(datetime.now())
print(zurichtime)

#-----------------------------------------------------------------------------------------------------#

#Mit Zeitzonen rechnen
#ausgehend von einem Zeitpunkt kann man mithilfe von timedelta Obejkten das Datum errechnen
datenow = datetime.now().date()
week = timedelta(weeks=1)

tw = today + week
tw3 = today + 3 * week

print(datenow)
print(tw, tw3)

#-----------------------------------------------------------------------------------------------------#

#Zeiten subtrahieren
today = date.today()
christmas = date(today.year, 12, 24)
wait = christmas - today
print('Noch', wait.days, "Tage bis Weihnachten")


